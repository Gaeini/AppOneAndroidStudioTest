package ir.syborg.app.apponestudio;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;

public class SecondActivity extends AppCompatActivity {

  public static final String TAG="Man";
  public static final String SD_CARD= Environment.getExternalStorageDirectory().getAbsolutePath();
  public static final String ADDRESS_DB=SD_CARD+"/gaeinicom/";
  public static SQLiteDatabase DB;
  private static AtomicInteger count = new AtomicInteger(0);
  int uniqueID=0;
  String strGender;
  String strName;
  String strFamily;
  String strAge;
  RadioButton rdoBtn;

  @Override
  protected void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);

    setContentView(R.layout.second_activity);

    uniqueID = count.incrementAndGet();
    TextView txt = (TextView) findViewById(R.id.getIntent);
    final EditText edtName = (EditText) findViewById(R.id.edtName);
    final EditText edtFamily = (EditText) findViewById(R.id.edtFamily);
    final EditText edtAge = (EditText) findViewById(R.id.edtAge);
    final EditText edtId=(EditText) findViewById(R.id.edtId);
    final RadioGroup rdoGender = (RadioGroup) findViewById(R.id.rdoGender);
    final RadioButton rdoMale=(RadioButton) findViewById(R.id.rdoMale);
    final RadioButton rdoFemale=(RadioButton) findViewById(R.id.rdoFemale);

    Button btnSend = (Button) findViewById(R.id.btnSend);
    Button btnDel = (Button) findViewById(R.id.btnDel);
    Button btnEdt = (Button) findViewById(R.id.btnEdt);
    Button btnSel = (Button) findViewById(R.id.btnSel);


    rdoGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
      @Override
      public void onCheckedChanged(RadioGroup group, int checkedId) {
        final int rdoId = rdoGender.getCheckedRadioButtonId();
        rdoBtn = (RadioButton) findViewById(rdoId);
        strGender = rdoBtn.getText().toString();
        Toast.makeText(SecondActivity.this, strGender, Toast.LENGTH_SHORT).show();
      }
    });


    String str = getIntent().getExtras().getString("name");
    txt.setText(str);

    new File(ADDRESS_DB).mkdir();

    DB = SQLiteDatabase.openOrCreateDatabase(ADDRESS_DB + "/datame.sqlite", null);
    DB.execSQL("CREATE  TABLE  IF NOT EXISTS personal (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL ," +
      " name VARCHAR, family VARCHAR, age INTEGER, gender VARCHAR)");


    btnSend.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        strName = edtName.getText().toString().trim();
        strFamily = edtFamily.getText().toString().trim();
        strAge = edtAge.getText().toString().trim();
          edtId.setText(" - ");
        if (strName.length() == 0 || strFamily.length() == 0 || strAge.length() == 0 || strGender == null) {
          Toast.makeText(SecondActivity.this, "Pleas Comlete All Fild ", Toast.LENGTH_SHORT).show();
        } else {
          DB.execSQL("INSERT INTO personal(name,family,age,gender) VALUES" +
            " ('" + edtName.getText() + "','" + edtFamily.getText() + "','" + edtAge.getText() + "','" + strGender + "')");
          Toast.makeText(SecondActivity.this, "Data Sent!!! ", Toast.LENGTH_SHORT).show();
        }

      }
    });

    btnDel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                  DB.execSQL("DELETE FROM personal WHERE id='"+edtId.getText()+"'");
                                  Toast.makeText(SecondActivity.this, "Data Sent!!! ", Toast.LENGTH_SHORT).show();

                                  edtName.setText("");
                                  edtFamily.setText("");
                                  edtAge.setText("");
                                  edtId.setText("");
                                  rdoFemale.setChecked(false);
                                  rdoMale.setChecked(false);
                                }


                              });

    btnEdt.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (edtId.getText().length()==0) {
          Toast.makeText(SecondActivity.this, "لطفا کد شناسایی را وارد کنید!", Toast.LENGTH_SHORT).show();
        } else {
          DB.execSQL("UPDATE personal SET name='"+edtName.getText()+"', family='"+edtFamily.getText()+"',age='"+edtAge.getText()+"',gender='"+strGender+"'"+
            " WHERE id='"+edtId.getText()+"'");
          Toast.makeText(SecondActivity.this, "Data Sent!!! ", Toast.LENGTH_SHORT).show();
          Log.i(TAG, "onClick: ");
          Log.e(TAG, "onClick: Yes" );
        }


      }
    });


    btnSel.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        Cursor cursor=DB.rawQuery("select * from personal where id='"+edtId.getText()+"'",null);


        if(cursor.moveToFirst()) {
          if(cursor.getString(0).length()==0){
            Toast.makeText(SecondActivity.this, "این اطلاعات وجود ندارد یا حذف شده!", Toast.LENGTH_SHORT).show();
          }
          edtName.setText(cursor.getString(1));
          edtFamily.setText(cursor.getString(2));
          edtAge.setText(cursor.getString(3));

            if((cursor.getString(4).equals("Female"))) {
              rdoFemale.setChecked(true);
            } else if((cursor.getString(4).equals("Male"))) {
              rdoMale.setChecked(true);
            }
        }
      }
    });


  }
  }

