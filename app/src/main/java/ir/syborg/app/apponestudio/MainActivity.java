package ir.syborg.app.apponestudio;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.text.DateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

  private static final String TAG = "Man";
  int ID =0;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main_activity);


    TextView date=(TextView) findViewById(R.id.date);
    Button button=(Button) findViewById(R.id.button);
    Button btnWeb=(Button) findViewById(R.id.btnWeb);
    final ListView listView=(ListView) findViewById(R.id.listView);

    btnWeb.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.gaeini.com"));
        startActivity(intent);
      }
    });





    button.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        ID++;
        if (ID <3) {
          String[] name = new String[]{"Mohammad", "Amir", "Hasan", "Akbar", "Afsane", "Reyhane", "Fateme", "Zahra"};
          ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this, R.layout.layout_adapter, R.id.textView2, name);
          listView.setAdapter(adapter);
          listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              TextView txt = (TextView) view.findViewById(R.id.textView2);
              String stxt = txt.getText().toString();
              Intent intent = new Intent(MainActivity.this, SecondActivity.class);
              intent.putExtra("name", stxt);
              startActivity(intent);
              Toast.makeText(MainActivity.this, stxt, Toast.LENGTH_SHORT).show();

            }
          });

        } else {
          String[] shahr = getResources().getStringArray(R.array.shahr);
          ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, shahr);
          listView.setAdapter(adapter);
          listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              TextView txt = (TextView) view.findViewById(android.R.id.text1);
              String stxt = txt.getText().toString();
              Toast.makeText(MainActivity.this, stxt, Toast.LENGTH_SHORT).show();

            }
          });
        }
      }});




    String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
    date.setText(currentDateTimeString);
    Log.i("Man" , "Date Is " + currentDateTimeString);


    isPrim(7);
    isPrim(21);
  }

  public void isPrim(double num) {
    for(int i=2;i<num;i++) {
      if(num%i==0) {
        Log.i("Man", num + " Is Not Prime");
        return;
      }
    }
    Log.i(TAG,num+" Is Prime");
  }



}
